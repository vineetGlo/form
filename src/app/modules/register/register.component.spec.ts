/*
 * Testing a simple Angular 2 component
 * More info: https://angular.io/docs/ts/latest/guide/testing.html#!#simple-component-test
 */

import { TestBed, ComponentFixture, async } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Http, HttpModule } from '@angular/http';
import { Component, DebugElement, NO_ERRORS_SCHEMA} from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { RegisterComponent } from './register.component';
import { RegisterService } from './register.service';
import { APP_CONFIG, APP_CONFIG_API_ENDPOINTS } from '../../app.config';

describe('Register Component', () => {
    let comp:            RegisterComponent;
    let fixture:         ComponentFixture<RegisterComponent>;
    let de:              DebugElement;
    let el:              HTMLElement;
    let spy:             jasmine.Spy;
    let registerService: RegisterService;

    beforeEach( async(() => {
        TestBed.configureTestingModule({
            imports: [CommonModule, ReactiveFormsModule, HttpModule],
            declarations: [ RegisterComponent ],
            providers: [
                RegisterService,
                FormBuilder,
                {provide:APP_CONFIG, useValue:APP_CONFIG_API_ENDPOINTS}
            ],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(RegisterComponent);
        comp = fixture.componentInstance;
        de = fixture.debugElement.query(By.css('.registerForm'));
        el = de.nativeElement;
        registerService = fixture.debugElement.injector.get(RegisterService);

        // Setup spy on the `getQuote` method
        spy = spyOn(registerService, 'save').and.returnValue(Promise.resolve({
            first_name: 'FIRST',
            last_name: 'LAST'
        }));
    });

    it('should show formTitle as Register Now', () => {
        fixture.detectChanges();        // update view with quote
        let formTitle = el.querySelector('h3');
        expect(formTitle.textContent).toBe('Register Now!');
    });

    it('validating First name field with no errors', () => {
        comp.registerModel.first_name = "FIRST";
        fixture.detectChanges();        // update view with quote
        let firstNameError;
        firstNameError = el.querySelector('.first_name_alert');
        expect(firstNameError).toBeUndefined;
    });

     it('validating Last name field with no errors', () => {
        comp.registerModel.first_name = "LAST";
        fixture.detectChanges();        // update view with quote
        let lastNameError = el.querySelector('.last_name_alert');
        expect(lastNameError).toBeUndefined;
    });

    it('should call service save function', () => {
        comp.registerModel.first_name = "FIRST";
        comp.registerModel.last_name = "LAST";
        fixture.detectChanges();
        comp.onSubmit();
        expect(spy.calls.any()).toBe(true, 'Register Servcie Save Called');
    });

    it('should show user Object aftersave promise (async)', async(() => {
        comp.registerModel.first_name = "FIRST";
        comp.registerModel.last_name = "LAST";
        fixture.detectChanges();
        comp.onSubmit();
        expect(registerService.save).toHaveBeenCalled();
    }));

});
