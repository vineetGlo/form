import * as console from 'console';
import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { RegisterModel } from './register.model';
import { RegisterService } from './register.service';

@Component({
    moduleId: 'register',
    selector: 'register',
    templateUrl: './register.component.html',
    styles: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
    public registerModel: RegisterModel = new RegisterModel();
    public registerForm: FormGroup;

    constructor(
        private fb: FormBuilder,
        private registerService: RegisterService
    ) { }

    ngOnInit(): void {
        this.buildForm();
    }

    buildForm(): void {
        this.registerForm = this.fb.group({
            'first_name': [ this.registerModel.first_name, [
                    Validators.minLength(4),
                    Validators.maxLength(24)
                ]
            ],
            'last_name': [ this.registerModel.last_name, [
                    Validators.required,
                    Validators.minLength(4),
                    Validators.maxLength(24)
                ]
            ]
        });
        this.registerForm.valueChanges
            .subscribe(data => this.onValueChanged(data));
        this.onValueChanged(); // (re)set validation messages now
    }

    onValueChanged(data?: any) {
        if (!this.registerForm) { return; }
        const form = this.registerForm;

        for (const field in this.formErrors) {
            // clear previous error message (if any)
            this.formErrors[field] = '';
            const control = form.get(field);

            if (control && control.dirty && !control.valid) {
                const messages = this.validationMessages[field];
                for (const key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
    }

    public formErrors = {
        'first_name': '',
        'last_name': ''
    };

    public validationMessages = {
        'first_name': {
            'minlength': 'First Name must be at least 4 characters long.',
            'maxlength': 'First Name cannot be more than 24 characters long.'
        },
        'last_name': {
            'required': 'Last Name is required.',
            'minlength': 'Last Name must be at least 4 characters long.',
            'maxlength': 'Last Name cannot be more than 24 characters long.'
        }
    };

    public onSubmit() {
       window.console.log(this.registerModel);
       this.registerService.save(this.registerModel).then((response)=>{
            window.console.log(response);
       });
    }
}