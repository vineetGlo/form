import { Injectable, Inject } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { APP_CONFIG, APP_CONFIG_API_ENDPOINTS } from '../../app.config'
import { RegisterModel } from './register.model';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class RegisterService {

    private registerUrl = this.appConfig.register;
      
    constructor(
        private http: Http,
        @Inject(APP_CONFIG) public appConfig: any
    ){}

    save(registerModel: RegisterModel): Promise<RegisterModel>  {
        let headers = new Headers({'Content-Type': 'application/json'});
        return this.http
            .post(this.registerUrl, JSON.stringify(registerModel), {headers: headers})
            .toPromise()
            .then(res => res.json().data as RegisterModel)
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        window.console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}