import { NgModule } from '@angular/core';
import { RegisterRoutingModule }   from './register.routing.module';
import { RegisterComponent } from './register.component';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { RegisterService } from './register.service';

@NgModule({
    imports: [CommonModule, RegisterRoutingModule, ReactiveFormsModule],
    declarations: [RegisterComponent],
    providers: [RegisterService],
})
export class RegisterModule {
    constructor() {}
}
