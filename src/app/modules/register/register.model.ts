// REGISTER MODEL
export class RegisterModel {

    constructor(
        public first_name?: string,
        public last_name : string = ''
    ) {}
}