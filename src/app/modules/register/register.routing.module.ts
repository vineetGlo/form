import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RegisterComponent } from './register.component';

const registerModuleRoutes: Routes = [
  { path: 'register', component: RegisterComponent },
];

@NgModule({
  imports: [RouterModule.forChild(registerModuleRoutes)],
  exports: [RouterModule],
})
export class RegisterRoutingModule { }