import { OpaqueToken } from '@angular/core';
export const APP_CONFIG =  new OpaqueToken('app.config');
export const APP_CONFIG_API_ENDPOINTS =  {
    register: '/register'
};