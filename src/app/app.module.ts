import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app.routing.module';
import { AppComponent } from './app.component';
import { RegisterModule } from './modules/register/register.module';
import { APP_CONFIG, APP_CONFIG_API_ENDPOINTS } from './app.config'
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AppRoutingModule,
    RegisterModule
  ],
  providers: [{provide:APP_CONFIG, useValue:APP_CONFIG_API_ENDPOINTS}],
  bootstrap: [AppComponent]
})
export class AppModule { }
