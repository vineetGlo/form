# README #

**FORM APP** will just show the registration form with two fields ie First Name and Last Name. Last name field will be mandatory.

### What is this repository for? ###

* This app will just show the registration form with two fields ie First Name and Last Name. Last name field will be mandatory.
* Version: 1.0.0

### How do I get set up? ###
* Just Need to make **npm install** commond
* Dependencies: NODE JS SHOULD BE INSTALL
* Database configuration: No Database is used
* How to run tests : ***ng test***
* Deployment instructions: Just clone and start ***npm install from root folder***
after that run the app with ***ng serve***
You may check the app open in url http://localhost:4200/
### Who do I talk to? ###
* Repo owner: Vineet Verma [vineet07verma@gmail.com]